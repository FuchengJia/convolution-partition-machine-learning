
import numpy as np

class DataType:
  IN_SHAPE = 7
  FILTER_SHAPE = 8
  STRIDES_SHAPE = 10
  DILATIONS_SHAPE = 13
  CS_SHAPE = 14
  FREQ_SHAPE = 15
  P_RATIO = 16

class DataLoader(object):
  def __init__(self, filename):
    self.filename = filename

    data_list = []
    pred_list = []
    for line in open(filename):
      if not line.startswith("op"):
        continue

      # Split string
      data = self.split_keep_shape(line[3:])
      #line = remove_unused_symbol(line)
      #data = line.split(',')
      #print(data)
      
      # Get data item
      batch = float(data[DataType.IN_SHAPE][0])
      in_size = float(data[DataType.IN_SHAPE][1])
      in_channel = float(data[DataType.IN_SHAPE][3])
      out_channel = float(data[DataType.FILTER_SHAPE][0])
      filter_size = float(data[DataType.FILTER_SHAPE][2])
      strides = float(data[DataType.STRIDES_SHAPE][0])
      dilations = float(data[DataType.DILATIONS_SHAPE][0])
      cs_cpu_bg_n = float(data[DataType.CS_SHAPE][0])
      cs_cpu_bg_h = float(data[DataType.CS_SHAPE][1])
      cs_gpu = float(data[DataType.CS_SHAPE][2])
      freq_cpu_bg_n = float(data[DataType.FREQ_SHAPE][0])
      freq_cpu_bg_h = float(data[DataType.FREQ_SHAPE][1])
      freq_gpu = float(data[DataType.FREQ_SHAPE][2])
      p_ratio = float(data[DataType.P_RATIO][0])
      
      # Append to list
      data_list.append([batch, in_size, in_channel, out_channel,
                        filter_size, strides, dilations,
                        cs_cpu_bg_n, cs_cpu_bg_h, cs_gpu,
                        freq_cpu_bg_n, freq_cpu_bg_h, freq_gpu])
      pred_list.append([p_ratio])

    self.data_dict = {}
    self.data_dict['data'] = np.array(data_list)
    self.data_dict['pred'] = np.array(pred_list)

  def split_keep_shape(self, line):
    target_list = []
    sub_list = []
    data_list = line.split(',')
    for data in data_list:
      if data.startswith('['):
        sub_list.append(data[1:])
      elif data.endswith(']'):
        sub_list.append(data[:-1])
        target_list.append(sub_list)
        sub_list = []
      else:
        sub_list.append(data)
        if len(sub_list) == 1:
          target_list.append(sub_list)
          sub_list = []

    return target_list

  #def remove_unused_symbol(self, line):
  #  return line

  def get_data(self, data_name):
    return self.data_dict[data_name]

if __name__ == '__main__':
  dl = DataLoader('data/train_data_01.txt')
  print(dl.get_data('data'))
  print(dl.get_data('pred'))
