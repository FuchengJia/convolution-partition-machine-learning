
import tensorflow as tf
from sklearn import preprocessing
from data_loader import DataLoader

def test():

  filename = 'data/train_data_01.txt'
  data_loader = DataLoader(filename)
  x_data = data_loader.get_data('data')
  y_pred = data_loader.get_data('pred')

  n_testing_example = x_data.shape[0]
  n_features = x_data.shape[1]

  print('input shape: {}'.format(x_data.shape))
  print(x_data)

  # Data scaling
  mm_scaler = preprocessing.MinMaxScaler()
  x_data = mm_scaler.fit_transform(x_data)
  print('input shape: {}'.format(x_data.shape))
  print(x_data)
  
  input_data = tf.placeholder(tf.float32, [None,n_features], name='input')
  predicition = tf.placeholder(tf.float32, [None,1], name='prediction')

  # Build model
  output = tf.layers.dense(input_data, 1)

  BATCH_SIZE = 1

  si = 0
  ei = BATCH_SIZE

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
  config = tf.ConfigProto(gpu_options=gpu_options, allow_soft_placement=True)
  with tf.Session(config=config) as sess:
    sess.run(tf.global_variables_initializer())
    # Restore weights
    model_steps = '100000'
    model_name = 'model_ckpt/pr_predictor-' + model_steps
    saver = tf.train.Saver()
    saver.restore(sess, model_name)
    for i in range(n_testing_example):
      pred_value = sess.run([output], feed_dict={input_data: [x_data[i]],
                                                 predicition: [y_pred[i]]})
      print('Pred {} GT {}'.format(pred_value[0], y_pred[i]))

if __name__ == '__main__':
  test()
