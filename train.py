
import time
import tensorflow as tf
from sklearn import preprocessing
from data_loader import DataLoader

def train():

  filename = 'data/train_data_01.txt'
  data_loader = DataLoader(filename)
  x_data = data_loader.get_data('data')
  y_pred = data_loader.get_data('pred')

  n_training_example = x_data.shape[0]
  n_features = x_data.shape[1]

  # Data scaling
  mm_scaler = preprocessing.MinMaxScaler()
  x_data = mm_scaler.fit_transform(x_data)
  print(x_data)
  
  input_data = tf.placeholder(tf.float32, [None,n_features], name='input')
  predicition = tf.placeholder(tf.float32, [None,1], name='prediction')

  # Build model
  output = tf.layers.dense(input_data, 1)
  
  # Define loss function and optimizer
  loss = tf.losses.mean_squared_error(predicition, output)
  optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.005)
  train_op = optimizer.minimize(loss)

  BATCH_SIZE = 4
  MAX_STEPS = 100000

  si = 0
  ei = BATCH_SIZE

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
  config = tf.ConfigProto(gpu_options=gpu_options, allow_soft_placement=True)
  saver = tf.train.Saver()
  time_start = time.time()
  with tf.Session(config=config) as sess:
    sess.run(tf.global_variables_initializer())
    for step in range(MAX_STEPS):
      _, loss_value, _, = sess.run([train_op, loss, output],
                                   feed_dict={input_data: x_data[si:ei],
                                              predicition: y_pred[si:ei]})
      if step % 10000 == 0:
        print('Step {} Loss {} Time {:.3f} sec'.format(
            step, loss_value, time.time() - time_start))
        time_start = time.time()

      si = ei
      if si == n_training_example:
        si = 0
      ei = si + BATCH_SIZE
      if ei > n_training_example:
        ei = n_training_example

    # Save checkpoint
    model_name = 'model_ckpt/pr_predictor'
    saver.save(sess, model_name, global_step=MAX_STEPS)
    print('Checkpoint saved to model_ckpt')

if __name__ == '__main__':
  train()
