
import tensorflow as tf
from tensorflow.python.framework import graph_util

def freeze_graph(input_checkpoint,
                 output_graph,
                 show_tensor_name=False):
  output_node_names = "dense/BiasAdd"
  saver = tf.train.import_meta_graph(input_checkpoint + '.meta',
                                     clear_devices=True)
  graph = tf.get_default_graph()
  input_graph_def = graph.as_graph_def()

  if show_tensor_name:
    print("Tensor Node Names")
    tensor_names = [tensor.name for tensor in input_graph_def.node]
    for name in tensor_names:
      print(name)
    return

  with tf.Session() as sess:
    saver.restore(sess, input_checkpoint)
    output_graph_def = graph_util.convert_variables_to_constants(
        sess=sess,
        input_graph_def=input_graph_def,
        output_node_names=output_node_names.split(","))
    with tf.gfile.GFile(output_graph, "wb") as f:
      f.write(output_graph_def.SerializeToString())
    print("PB model save to " + output_graph)

def main():
  steps = 100000
  show_tensor_name = False
  freeze_graph("model_ckpt/pr_predictor-" + str(steps),
               "model_pb/pr_predictor.pb",
               show_tensor_name)

if __name__ == "__main__":
  main()
